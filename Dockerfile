# The current docker file build the deep-linker service

FROM node:12.16.3-stretch-slim

WORKDIR /app
ADD . /app

ENV NODE_ENV="production"

RUN npm install && \
	npm run build


CMD [ "node", "build/server" ]
