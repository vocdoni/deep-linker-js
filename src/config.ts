import { existsSync, readFileSync } from "fs"
import { parse } from 'toml'
const assert = require("assert")

const CONFIG_FILE = "config.toml"

///////////////////////////////////////////////////////////////////////////////
// CONFIG
///////////////////////////////////////////////////////////////////////////////

assert(existsSync(CONFIG_FILE), "The config file " + CONFIG_FILE + " does not exist")
console.log("Reading settings from", CONFIG_FILE)

export const config: Config = parse(readFileSync("./" + CONFIG_FILE).toString())

assert(typeof config == "object", "The config file appears to be invalid")
assert(typeof config.http == "object", "The 'http' field should be an object")
assert(typeof config.redirect == "object", "The 'redirect' field should be an object")
assert(typeof config.link == "object", "The 'link' field should be an object")
assert(typeof config.android == "object", "The 'android' field should be an object")
assert(typeof config.ios == "object", "The 'ios' field should be an object")
assert(typeof config.social == "object", "The 'social' field should be an object")
assert(typeof config.misc == "object", "The 'misc' field should be an object")

assert(typeof config.http.port == "number", "config.toml > http.port should be a number")
assert(typeof config.redirect.domain == "string", "config.toml > redirect.domain should be a string")
assert(typeof config.link.prefix == "string", "config.toml > link.prefix should be a string")
assert(typeof config.link.fallback == "string", "config.toml > link.fallback should be a string")
assert(typeof config.android.package == "string", "config.toml > android.package should be a string")
assert(typeof config.android.scheme == "string", "config.toml > android.scheme should be a string")
assert(typeof config.android.defaultMinBuild == "number", "config.toml > android.defaultMinBuild should be a number")
assert(typeof config.ios.bundleId == "string", "config.toml > ios.bundleId should be a string")
assert(typeof config.ios.scheme == "string", "config.toml > ios.scheme should be a string")
assert(typeof config.ios.appStoreId == "string", "config.toml > ios.appStoreId should be a string")
assert(typeof config.ios.defaultMinVersion == "string", "config.toml > ios.defaultMinVersion should be a string")
assert(typeof config.social.title == "string", "config.toml > social.title should be a string")
assert(typeof config.social.description == "string", "config.toml > social.description should be a string")
assert(typeof config.social.image == "string", "config.toml > social.image should be a string")
assert(typeof config.misc.verbose == "boolean", "config.toml > misc.verbose should be a boolean")

// ENV overrides
if (process.env.HTTP_PORT) config.http.port = parseInt(process.env.HTTP_PORT)
if (process.env.REDIRECT_DOMAIN) config.redirect.domain = process.env.REDIRECT_DOMAIN
if (process.env.LINK_PREFIX) config.link.prefix = process.env.LINK_PREFIX
if (process.env.LINK_FALLBACK) config.link.fallback = process.env.LINK_FALLBACK
if (process.env.SOCIAL_TITLE) config.social.title = process.env.SOCIAL_TITLE
if (process.env.SOCIAL_DESCRIPTION) config.social.description = process.env.SOCIAL_DESCRIPTION

// Type export

type Config = {
    http: {
        port: number
    },
    redirect: {
        domain: string
    },
    link: {
        prefix: string
        fallback: string
    },
    android: {
        package: string
        scheme: string
        defaultMinBuild: number
    },
    ios: {
        bundleId: string
        scheme: string
        appStoreId: string
        defaultMinVersion: string
    },
    social: {
        title: string,
        description: string
        image: string
    },
    misc: {
        verbose: boolean
    }
}

export default config
