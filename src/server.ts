import config from "./config"
import { createServer } from "http"
import { makeLink, makeEntityLink, makeProcessLink, makePostLink, makeRegistryValidationLink } from "./links"

const app = createServer((req, res) => {
    if (config.misc.verbose) console.log(req.method, req.url)

    let link: string
    if (req.url.startsWith("/entities/")) {
        link = makeEntityLink(req.url)
    }
    else if (req.url.startsWith("/processes/")) {
        link = makeProcessLink(req.url)
    }
    else if (req.url.startsWith("/posts/")) {
        link = makePostLink(req.url)
    }
    else if (req.url.startsWith("/validation/")) {
        link = makeRegistryValidationLink(req.url)
    }
    else {
        link = makeLink(config.link.fallback)
    }

    res.writeHead(302, { 'Location': link })
    res.end()
})

app.listen(config.http.port, () => {
    console.log("Listening on port", config.http.port)
})
