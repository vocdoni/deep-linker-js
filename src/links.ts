import config from "./config"

export function makeLink(mainUrl: string,
    extra?: { minAndroidBuild?: number, minIosVersion?: string, title?: string, description?: string, image?: string }): string {

    const link = encodeURIComponent(mainUrl)
    const minSupportedAndroidBuild = extra && extra.minAndroidBuild || config.android.defaultMinBuild
    const minSupportedIosVersion = extra && extra.minIosVersion || config.ios.defaultMinVersion
    const title = encodeURIComponent(extra && extra.title || config.social.title)
    const description = encodeURIComponent(extra && extra.description || config.social.description)
    const image = encodeURIComponent(extra && extra.image || config.social.image)

    // EXAMPLE
    // https://vocdoni.link/?link=https%3A%2F%2Fwww.my-domain.org%2Flink%2Fentries%2F5ad48c1a095a936b16556c3f&apn=com.myapp&amv=9&ibi=org.my.app&ius=my-domain&isi=1010900960&ivm=1.1.0&st=Vocdoni&sd=Example%20description%20goes%20here+&si=https://www.my-domain.org/social.jpg
    return `https://${config.redirect.domain}/?link=${link}&apn=${config.android.package}&amv=${minSupportedAndroidBuild}&ibi=${config.ios.bundleId}&ius=${config.ios.scheme}&isi=${config.ios.appStoreId}&ivm=${minSupportedIosVersion}&st=${title}&sd=${description}&si=${image}`
}

export function makeEntityLink(url: string): string {
    if (!url) return null
    const params = url.replace(/^\/entities\//, "").split("/")
    if (!params[0]) return null
    const entityId = params[0]

    const webUrl = `${config.link.prefix}/entities/#/${entityId}`
    return makeLink(webUrl)
}

export function makeProcessLink(url: string): string {
    if (!url) return null
    const params = url.replace(/^\/processes\//, "").split("/")
    if (params.length < 2) return null
    else if (!params[0] || !params[1]) return null
    const entityId = params[0]
    const processId = params[1]

    const webUrl = `${config.link.prefix}/processes/#/${entityId}/${processId}`
    return makeLink(webUrl)
}

export function makePostLink(url: string): string {
    if (!url) return null
    const params = url.replace(/^\/posts\//, "").split("/")
    if (params.length < 2) return null
    else if (!params[0] || !params[1]) return null
    const entityId = params[0]
    const idx = params[1]

    const webUrl = `${config.link.prefix}/posts/#/${entityId}/${idx}`
    return makeLink(webUrl)
}

export function makeRegistryValidationLink(url: string): string {
    if (!url) return null
    const params = url.replace(/^\/validation\//, "").split("/")
    if (params.length < 2) return null
    else if (!params[0] || !params[1]) return null
    const entityId = params[0]
    const validationToken = params[1]

    const webUrl = `${config.link.prefix}/validation/#/${entityId}/${validationToken}`
    return makeLink(webUrl)
}
